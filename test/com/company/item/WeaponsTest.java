package com.company.item;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.util.ArmorType;
import com.company.util.Slot;
import com.company.util.WeaponType;

import com.company.character.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponsTest {
    //Appendix C: 3) Item and equipment tests. Part 1 - 4 Exceptions.
    //Part 1
    @Test
    void weapon_level_too_high() throws InvalidWeaponException {
        //Arrange
        Warrior per = new Warrior("Per");
        Weapons mythicalAxe = new Weapons("The Mythical Axe", 2,Slot.WEAPON, WeaponType.AXE, 3,1);

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            per.setWeapon(mythicalAxe);
        });
        String expected = "Your level is lower the minimum requiered for this weapon";
        String actual = exception.getMessage();
        //Assert
        assertTrue(actual.contains(expected));
    }

    //Part 2
    @Test
    void armor_level_too_high() throws InvalidArmorException {
        //Arrange
        Warrior per = new Warrior("Per");
        Armor platemail = new Armor("Dirty, scrapy and crappy Platemail", 2, Slot.BODY, ArmorType.PLATE, 1);

        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            per.setArmor(platemail);
        });
        String expected = "Your level is lower the minimum requiered for this armor";
        String actual = exception.getMessage();
        //Assert
        assertTrue(actual.contains(expected));
    }

    //Part 3
    @Test
    void warrior_wrong_weapon_equip() throws InvalidWeaponException {
        //Arrange
        Warrior per = new Warrior("Per");
        Weapons bow = new Weapons("The Mythical Bow", 1,Slot.WEAPON, WeaponType.BOW, 3,1);

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            per.setWeapon(bow);
        });
        String expected = "The weapon type: BOW is illegal for the warrior class";
        String actual = exception.getMessage();
        //Assert
        assertTrue(actual.contains(expected));
    }

    //Part 4
    @Test
    void warrior_wrong_armor_equip() throws InvalidArmorException {
        //Arrange
        Warrior per = new Warrior("Per");
        Armor clothhood = new Armor("the finest hood in the land", 1, Slot.HEAD, ArmorType.CLOTH, 1);

        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            per.setArmor(clothhood);
        });
        String expected = "The armor type: CLOTH is illegal for the warrior class";
        String actual = exception.getMessage();
        //Assert
        assertTrue(actual.contains(expected));
    }



    //Appendix C: 3) Item and equipment tests. Part 5 - 9, valid Items and DPS.
    //Part 5
    @Test
    void character_equips_valid_weapon() throws InvalidWeaponException{
        //Arrange
        Warrior per = new Warrior("Per");
        Weapons mythicalAxe = new Weapons("The Mythical Axe", 1,Slot.WEAPON, WeaponType.AXE, 3,1);
        //Act
        per.setWeapon(mythicalAxe);
        Weapons expected = ((Weapons)per.getEquipment(Slot.WEAPON));
        //Assert
        assertTrue(expected == mythicalAxe);
    }

    //Part 6
    @Test
    void character_equips_valid_armor() throws InvalidArmorException {
        //Arrange
        Warrior per = new Warrior("Per");
        Armor random_bucket = new Armor("A dirty cleaning bucket", 1,Slot.HEAD, ArmorType.PLATE, 1);
        //Act
        per.setArmor(random_bucket);
        Armor expected = ((Armor)per.getEquipment(Slot.HEAD));
        //Assert
        assertTrue(expected == random_bucket);
    }

    //Part 7
    @Test
    void dps_test_no_weapon(){
        //Arrange
        Warrior rut = new Warrior("Rut");
        double expected = 1*(1+(5.0/100));
        //Act
        double actual = rut.characterDPS();
        //Assert
        assertEquals(expected, actual);
    }

    //Part 8
    @Test
    void dps_test_warrior_with_basic_axe() throws InvalidWeaponException{
        //Arrange
        Warrior rut = new Warrior("Rut");
        Weapons axe = new Weapons("Butchers Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        rut.setWeapon(axe);
        double expected = (7*1.1)*(1+(5.0/100));
        //Act
        double actual = rut.characterDPS();
        //Assert
        assertEquals(expected, actual);
    }

    //Part 9
    @Test
    void dps_test_warrior_with_axe_and_platearmor() throws InvalidWeaponException, InvalidArmorException{
        //Arrange
        Warrior rut = new Warrior("Rut");
        Weapons axe = new Weapons("Butchers Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        Armor platemail = new Armor("Dirty, scrapy and crappy Platemail", 1, Slot.BODY, ArmorType.PLATE, 1);

        rut.setWeapon(axe);
        rut.setArmor(platemail);
        double expected = (7*1.1)*(1+((1+5.0)/100));
        //Act
        double actual = rut.characterDPS();
        //Assert
        assertEquals(expected, actual);
    }

}