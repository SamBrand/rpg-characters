package com.company.character;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    //Appendix C: 2) Character attribute and level tests.
    //Part 1
    @Test
    void check_If_Character_starts_at_level_one() {
        //Arrange
        Mage ole = new Mage("Ole");
        int expected = 1;
        //act
        int actual = ole.getLevel();
        //Assert
        assertEquals(expected, actual);

    }
    //Appendix C: 2) Character attribute and level tests.
    //Part 2
    @Test
    void check_characterLevel_2_after_levelUp() {
        //Arrange
        Mage ole = new Mage("Ole");
        int expected = 2;
        //Act
        ole.levelUp();
        int actual = ole.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


    //Appendix C: 2) Character attribute and level tests.
    //Part 3, Each character class is created with the proper default attributes.
    @Test
    void mage_start_stats_check() {
        //Arrange
        Mage ole = new Mage("Ole");
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedVitality = 5;
        int expectedInteligence = 8;
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void ranger_start_stats_check() {
        //Arrange
        Ranger ole = new Ranger("Ole");
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedVitality = 8;
        int expectedInteligence = 1;
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void rouge_start_stats_check() {
        //Arrange
        Rogue ole = new Rogue("Ole");
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedVitality = 8;
        int expectedInteligence = 1;
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void warrior_start_stats_check() {
        //Arrange
        Warrior ole = new Warrior("Ole");
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedVitality = 10;
        int expectedInteligence = 1;
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }


    //Appendix C: 2) Character attribute and level tests.
    // Part 4, all classes levelUp test
    @Test
    void mage_Update_stats_check() {
        //Arrange
        Mage ole = new Mage("Ole");
        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedVitality = 8;
        int expectedInteligence = 13;
        ole.levelUp();
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void ranger_Update_stats_check() {
        //Arrange
        Ranger ole = new Ranger("Ole");
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedVitality = 10;
        int expectedInteligence = 2;
        ole.levelUp();
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void rouge_Update_stats_check() {
        //Arrange
        Rogue ole = new Rogue("Ole");
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedVitality = 11;
        int expectedInteligence = 2;
        ole.levelUp();
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }
    @Test
    void warrior_Update_stats_check() {
        //Arrange
        Warrior ole = new Warrior("Ole");
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedVitality = 15;
        int expectedInteligence = 2;
        ole.levelUp();
        //Act
        int actualStrength = ole.getStrength();
        int actualDexterity = ole.getDexterity();
        int actualVitality = ole.getVitality();
        int actualIntelligence = ole.getIntelligence();
        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedVitality, actualVitality);
        assertEquals(expectedInteligence, actualIntelligence);
    }

}