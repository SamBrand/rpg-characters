package com.company;

import com.company.character.Mage;
import com.company.character.Warrior;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.Armor;
import com.company.item.Weapons;
import com.company.util.ArmorType;
import com.company.util.Slot;
import com.company.util.WeaponType;

public class Main {

    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {
	Mage hans = new Mage("Hans");
	Warrior greta = new Warrior("Greta");
	Weapons battleAxe = new Weapons("Battle Axe", 2, Slot.WEAPON, WeaponType.AXE, 3, 2);
	Armor chainmail = new Armor("Chain Mail", 3, Slot.BODY, ArmorType.MAIL, 3);
	Armor leatherBikini = new Armor("Leather Bikini", 5, Slot.BODY, ArmorType.LEATHER, 5);

	//hans.setArmor(chainmail);
	greta.levelUp();
	greta.levelUp();
	greta.setWeapon(battleAxe);
	greta.setArmor(chainmail);
    }
}
