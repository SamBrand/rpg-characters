package com.company.item;

import com.company.exceptions.InvalidArmorException;
import com.company.util.ArmorType;
import com.company.util.Slot;
import com.company.util.WeaponType;

public class Armor extends Item {
    private ArmorType armorType;
    private int bonus = 0;

    public Armor(String name, int minLevel, Slot slot, ArmorType armorType, int bonus) {
        super(name, minLevel, slot);
        this.armorType = armorType;
        this.bonus = bonus;
    }
    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public int getBonus(){
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
}
