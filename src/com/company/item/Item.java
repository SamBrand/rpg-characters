package com.company.item;

import com.company.util.Slot;

 public class Item {
    private String name;
    private int minLevel;
    private Slot slot;

    public Item(String name, int minLevel, Slot slot) {
        this.name = name;
        this.minLevel = minLevel;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(int minLevel) {
        this.minLevel = minLevel;
    }

    public Slot getSlot() {
        return slot;
    }

}
