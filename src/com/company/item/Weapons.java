package com.company.item;

import com.company.util.Slot;
import com.company.util.WeaponType;

public class Weapons extends Item{
    private WeaponType weaponType;
    private int damage;
    private double attackPS;

    public Weapons(String name, int minLevel, Slot slot, WeaponType weaponType, int damage, double attackPS) {
        super(name, minLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackPS = attackPS;
    }
    public WeaponType getWeaponType() {
        return weaponType;
    }
    public double damagePerSecond() {
        return damage*attackPS;
    }
}
