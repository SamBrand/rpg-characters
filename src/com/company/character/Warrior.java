package com.company.character;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.Armor;
import com.company.item.Weapons;
import com.company.util.ArmorType;
import com.company.util.WeaponType;

import static com.company.util.ArmorType.*;
import static com.company.util.WeaponType.*;

public class Warrior extends Character {
    public Warrior(String name) {
        setName(name);
        setLevel(1);
        setStrength(5);
        setDexterity(2);
        setIntelligence(1);
        setVitality(10);
        setPrimaryAttribute(getStrength());
    }

    @Override
    public void levelUp() {
        setLevel(getLevel() + 1);
        setVitality(getVitality() + 5);
        setStrength(getStrength() + 3);
        setDexterity(getDexterity() + 2);
        setIntelligence(getIntelligence() + 1);
    }
    @Override
    public void setArmor(Armor armor) throws InvalidArmorException {
        ArmorType armorType = armor.getArmorType();
        if (armorType != PLATE && armorType != MAIL){
            throw new InvalidArmorException("The armor type: " + armorType + " is illegal for the warrior class");
        } else {
            super.setArmor(armor);
            getEquipmentList().put(armor.getSlot(), armor);
        }
    }
    @Override
    public void setWeapon(Weapons weapon) throws InvalidWeaponException {
        WeaponType weaponType = weapon.getWeaponType();
        if (weaponType != AXE && weaponType != HAMMER && weaponType != SWORD){
            throw new InvalidWeaponException("The weapon type: " + weaponType + " is illegal for the warrior class");
        } else {
            super.setWeapon(weapon);
            getEquipmentList().put(weapon.getSlot(), weapon);
        }
    }
}
