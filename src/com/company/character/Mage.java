package com.company.character;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.Armor;
import com.company.item.Weapons;
import com.company.util.ArmorType;
import com.company.util.WeaponType;

import static com.company.util.ArmorType.CLOTH;
import static com.company.util.WeaponType.STAFF;
import static com.company.util.WeaponType.WAND;

public class Mage extends Character {
    public Mage(String name) {
        setName(name);
        setLevel(1);
        setStrength(1);
        setDexterity(1);
        setIntelligence(8);
        setVitality(5);
        setPrimaryAttribute(getIntelligence());
    }

    @Override
    public void levelUp() {
        setLevel(getLevel() + 1);
        setVitality(getVitality() + 3);
        setStrength(getStrength() + 1);
        setDexterity(getDexterity() + 1);
        setIntelligence(getIntelligence() + 5);
    }
    @Override
    public void setArmor(Armor armor) throws InvalidArmorException {
        ArmorType armorType = armor.getArmorType();
        if (armorType != CLOTH){
            throw new InvalidArmorException("The armor type " + armorType + " is illegal for the Mage class");
        } else {
        super.setArmor(armor);
            getEquipmentList().put(armor.getSlot(), armor);
        }
    }
    @Override
    public void setWeapon(Weapons weapon) throws InvalidWeaponException {
        WeaponType weaponType = weapon.getWeaponType();
        if (weaponType != STAFF && weaponType != WAND){
            throw new InvalidWeaponException("The weapon type: " + weaponType + " is illegal for the Mage class");
        } else {
            super.setWeapon(weapon);
            getEquipmentList().put(weapon.getSlot(), weapon);
        }
    }
}
