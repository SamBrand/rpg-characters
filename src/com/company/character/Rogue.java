package com.company.character;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.Armor;
import com.company.item.Weapons;
import com.company.util.ArmorType;
import com.company.util.WeaponType;

import static com.company.util.ArmorType.*;
import static com.company.util.WeaponType.*;

public class Rogue extends Character {
    public Rogue(String name) {
        setName(name);
        setLevel(1);
        setStrength(2);
        setDexterity(6);
        setIntelligence(1);
        setVitality(8);
        setPrimaryAttribute(getDexterity());
    }

    @Override
    public void levelUp() {
        setLevel(getLevel() + 1);
        setVitality(getVitality() + 3);
        setStrength(getStrength() + 1);
        setDexterity(getDexterity() + 4);
        setIntelligence(getIntelligence() + 1);
    }

    @Override
    public void setArmor(Armor armor) throws InvalidArmorException {
        ArmorType armorType = armor.getArmorType();
        if (armorType != LEATHER && armorType != MAIL){
            throw new InvalidArmorException("The armor type: " + armorType + " is illegal for the warrior class");
        } else {
            super.setArmor(armor);
            getEquipmentList().put(armor.getSlot(), armor);
        }
    }
    @Override
    public void setWeapon(Weapons weapon) throws InvalidWeaponException {
        WeaponType weaponType = weapon.getWeaponType();
        if (weaponType != DAGGER && weaponType != SWORD){
            throw new InvalidWeaponException("The weapon type: " + weaponType + " is illegal for the warrior class");
        } else {
            super.setWeapon(weapon);
            getEquipmentList().put(weapon.getSlot(), weapon);
        }
    }
}
