package com.company.character;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.Armor;
import com.company.item.Weapons;
import com.company.item.Item;
import com.company.util.Slot;

import java.util.HashMap;

abstract class Character {
    private String name;
    private int level;
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;
    private int primaryAttribute;
    private HashMap<Slot, Item> equipment = new HashMap<Slot, Item>();

    public void levelUp(){}

    public double totalAtribute(){
        return getPrimaryAttribute() +
                getEquipedItemBonus(Slot.HEAD) +
                getEquipedItemBonus(Slot.BODY) +
                getEquipedItemBonus(Slot.LEGS);
    }
    public double characterDPS(){
        return ((totalAtribute()/100)+1)*getEquipedItemBonus(Slot.WEAPON);
    }
    //Checking if Item is null and if Item is Weapon or Armor, and then sending bonus
    public double getEquipedItemBonus(Slot itemType){
        Item item = getEquipment(itemType);
        if (item == null && itemType == Slot.WEAPON){
            return 1;
        } else if (item == null) {
            return 0;
        }
        if (item.getSlot() == Slot.WEAPON){
            return ((Weapons)item).damagePerSecond();
        } else {
            return ((Armor)item).getBonus();
        }
    }

    public Item getEquipment(Slot itemType) {
        return equipment.get(itemType);
    }

    public HashMap<Slot, Item> getEquipmentList() {
        return equipment;
    }
    //All classes must check for level equal to armor level.
    public void setArmor(Armor armor) throws InvalidArmorException {
        int minLevel = armor.getMinLevel();
        if (this.getLevel() < minLevel){
            throw new InvalidArmorException("Your level is lower the minimum requiered for this armor");
        }
        return;
    }
    //All classes must check for level equal to weapon level.
    public void setWeapon(Weapons weapon) throws InvalidWeaponException {
        int minLevel = weapon.getMinLevel();
        if (this.getLevel() < minLevel){
            throw new InvalidWeaponException("Your level is lower the minimum requiered for this weapon");
        }
        return;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                ", vitality=" + vitality +
                ", primaryAttribute=" + primaryAttribute +
                ", equipment=" + equipment +
                '}';
    }

    //All functions below are custom getters and setters from intellij's generate function

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getPrimaryAttribute() {
        return primaryAttribute;
    }

    public void setPrimaryAttribute(int primaryAttribute) {
        this.primaryAttribute = primaryAttribute;
    }
}
