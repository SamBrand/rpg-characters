package com.company.exceptions;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String message) {
        super(message);
    }
}
