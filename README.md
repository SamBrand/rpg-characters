##RGB-Characters

Project that simulates the inner workings of some RPG-characters.

Note to Nick:
inside folder src/com.company there are four subfolder(character, exceptions, item, util) which contains all src code.

- character contains:
  - Character, an abstract class which is the parent of all other classes in this folder. It has the backbone functions that for getting totalAttributes and CharacterDPS.
  - Classes Mage, Ranger, Rogue and Warrior extends character, and all overrides the following functions for their individual up-dates and stats: levelUp(), setArmor(Armor) and setWeapon(Weapon).
  
- exceptions contains:
  - InvalidArmorException, it is used in the character classes and in testing.
  - InvalidWeaponException, it is used in the character classes and in testing.

- item contains:
  - Item, the parent class for Armor and Weapon.
  - Armor, extends Item and uses the enum ArmorType to identify the different kinds of armors. Also hold the int bonus that symbolizes the improvements when equiping armor.
  - Weapon, extends Item and uses the enum WeaponType to identify the different kinds of weapons. Also holds the values for damage and attacksPerSecond tha is used for the function damagePerSecond.

- util contains:
  - ArmorType, enum that holds the different armors.
  - WeaponType, enum that holds the different weapons.
  - Slot, enum that holds the different slots.

##Testing
 - Character attributes and level tests are all put in the MageTest.java file. All tests passes.

 - Item and equipment tests are all put in the WeaponsTest.java file. All tests passes.
